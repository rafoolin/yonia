# yonia

Yonia is a food recipe mobile app.

# To-do

- [ ] Code the UI.
  - [x] Sign in/Sign up(Portrait and Landscape).
- [ ] API calls.
- [ ] Dark/light theme.
- [ ] Offline database.
- [ ] Enabling user customization.

# Licences

- API is from [Edamam's Recipe Search API][api].
- UI/UX is from [“Yonia” food recipes iOS mobile app design (FREE PSD)][ui] by [Ashadul Islam Samiul (Sam)][ashadulislamsamiul].

<!-- Links  -->

[api]: https://developer.edamam.com/edamam-recipe-api
[ui]: https://www.behance.net/gallery/54837211/Yonia-food-recipes-iOS-mobile-app-design-%28FREE-PSD%29
[ashadulislamsamiul]: https://www.behance.net/AshadulislamSamiul

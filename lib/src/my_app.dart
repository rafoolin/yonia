import 'package:flutter/material.dart';
import 'pages/pages.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: _onGenerateRoute,
    );
  }
}

Route _onGenerateRoute(RouteSettings settings) {
  switch (settings.name) {

    // StartPage
    case StartPage.routeName:
      return MaterialPageRoute(
        builder: (BuildContext context) => const StartPage(),
      );
      break;

    // SignInPage
    case SignInPage.routeName:
      return MaterialPageRoute(
        builder: (BuildContext context) => const SignInPage(),
      );
      break;

    // ForgotPage
    case ForgotPage.routeName:
      return MaterialPageRoute(
        builder: (BuildContext context) => const ForgotPage(),
      );
      break;

    // SentEmailPage
    case SentEmailPage.routeName:
      return MaterialPageRoute(
        builder: (BuildContext context) => const SentEmailPage(),
      );
      break;

    // SignUPPage
    case SignUPPage.routeName:
      return MaterialPageRoute(
        builder: (BuildContext context) => const SignUPPage(),
      );
      break;

    // CountryPage
    case CountryPage.routeName:
      return MaterialPageRoute(
        builder: (BuildContext context) => const CountryPage(),
      );
      break;

    // VerificationPage
    case VerificationPage.routeName:
      return MaterialPageRoute(
        builder: (BuildContext context) => const VerificationPage(),
      );
      break;

    // PhoneVerifyPage
    case PhoneVerifyPage.routeName:
      return MaterialPageRoute(
        builder: (BuildContext context) => const PhoneVerifyPage(),
      );
      break;

    // AutoLoginPage
    case AutoLoginPage.routeName:
      return MaterialPageRoute(
        builder: (BuildContext context) => const AutoLoginPage(),
      );
      break;

    case SplashPage.routeName:
    default:
      return MaterialPageRoute(
        builder: (BuildContext context) => SplashPage(),
      );
      break;
  }
}

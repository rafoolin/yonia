import 'package:flutter/material.dart';
import 'package:yonia/src/configs/configs.dart';
import 'package:yonia/src/pages/pages.dart';

class ForgotPage extends StatelessWidget {
  static const String routeName = '/ForgotPage';
  const ForgotPage();
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => orientation == Orientation.portrait
          ? PortraitForgotPage()
          : LandscapeForgotPage(),
    );
  }
}

// ================================================================================
// =                              PORTRAITFORGOTPAGE                              =
// ================================================================================
class PortraitForgotPage extends StatefulWidget {
  const PortraitForgotPage();
  @override
  _PortraitForgotPageState createState() => _PortraitForgotPageState();
}

class _PortraitForgotPageState extends State<PortraitForgotPage> {
  GlobalKey<ScaffoldState> _key;
  @override
  void initState() {
    _key = GlobalKey();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _key,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: MediaQuery.of(context).padding.top + 16.0,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: width * 0.23),
              height: height * 0.037,
              child: FittedBox(
                child: Text(
                  'Forgot password',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.01)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              height: height * 0.092,
              child: FittedBox(
                child: Text(
                  'Introducing Yonia, with more than 6 thousand\n' +
                      'recipes and amazing features yonia is your\n' +
                      'best choice to make any cook great.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.05)),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.062),
              child: TextField(
                keyboardType: TextInputType.emailAddress,
                autofocus: false,
                decoration: InputDecoration(
                  labelText: 'Email',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                    fontSize: 15.0,
                  ),
                  hintText: 'user@example.com',
                  hintStyle: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
                style: TextStyle(color: BaseColor.red),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.04)),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: width * 0.062),
              padding: EdgeInsets.zero,
              height: height * 0.078,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [BaseColor.red, BaseColor.orange],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
              child: MaterialButton(
                padding: EdgeInsets.zero,
                child: Text(
                  'CHANGE PASSWORD',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                elevation: 0.0,
                onPressed: () =>
                    Navigator.of(context).pushNamed(SentEmailPage.routeName),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 16.0),
              child: GestureDetector(
                child: Center(
                  child: Text(
                    'Resend email',
                    style: TextStyle(
                      fontSize: 13,
                      color: Colors.black54,
                    ),
                  ),
                ),
                onTap: () {
                  _key.currentState.showSnackBar(
                      SnackBar(content: Text('We sent you another email')));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// ================================================================================
// =                              LANDSCAPEFORGOTPAGE                             =
// ================================================================================
class LandscapeForgotPage extends StatefulWidget {
  const LandscapeForgotPage();
  @override
  _LandscapeForgotPageState createState() => _LandscapeForgotPageState();
}

class _LandscapeForgotPageState extends State<LandscapeForgotPage> {
  GlobalKey<ScaffoldState> _key;

  @override
  void initState() {
    _key = GlobalKey();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      key: _key,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: width * 0.23),
              height: height * 0.08,
              child: FittedBox(
                fit: BoxFit.contain,
                child: Text(
                  'Forgot password',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: height * 0.18,
              margin: EdgeInsets.only(
                top: 16.0,
                left: 16.0,
                right: 16.0,
              ),
              child: FittedBox(
                child: Text(
                  'Introducing Yonia, with more than 6 thousand\n' +
                      'recipes and amazing features yonia is your\n' +
                      'best choice to make any cook great.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(
                top: 8.0,
                left: 16.0,
                right: 16.0,
              ),
              child: TextField(
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: 'Email',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                    fontSize: 15.0,
                  ),
                  hintText: 'user@example.com',
                  hintStyle: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
                style: TextStyle(color: BaseColor.red),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              width: width * 0.5,
              margin: EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              padding: EdgeInsets.only(top: 16.0),
              child: Container(
                padding: EdgeInsets.zero,
                width: width * 0.25,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [BaseColor.red, BaseColor.orange],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                ),
                child: MaterialButton(
                  padding: EdgeInsets.zero,
                  child: Text(
                    'CHANGE PASSWORD',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  elevation: 0.0,
                  onPressed: () =>
                      Navigator.of(context).pushNamed(SentEmailPage.routeName),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 16.0),
              child: GestureDetector(
                child: Center(
                  child: Text(
                    'Resend email',
                    style: TextStyle(
                      fontSize: 13,
                      color: Colors.black54,
                    ),
                  ),
                ),
                onTap: () {
                  _key.currentState.showSnackBar(
                      SnackBar(content: Text('We sent you another email')));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:yonia/src/configs/configs.dart';
import 'package:yonia/src/pages/pages.dart';

class SignUPPage extends StatelessWidget {
  static const String routeName = '/SignUPPage';
  const SignUPPage();
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => orientation == Orientation.portrait
          ? PortraitSignUPPage()
          : LandscapeSignUPPage(),
    );
  }
}

// ================================================================================
// =                              PORTRAITSIGNUPPAGE                              =
// ================================================================================
class PortraitSignUPPage extends StatefulWidget {
  const PortraitSignUPPage();
  @override
  _PortraitSignUPPageState createState() => _PortraitSignUPPageState();
}

class _PortraitSignUPPageState extends State<PortraitSignUPPage> {
  bool _obscureText;
  @override
  void initState() {
    _obscureText = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(height: MediaQuery.of(context).padding.top),
          ),
          SliverToBoxAdapter(
            child: Image.asset(
              'assets/images/start.png',
              height: height * 0.18,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: height * 0.045,
              child: FittedBox(
                child: Text(
                  'Sign up',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.01)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              height: height * 0.092,
              child: FittedBox(
                child: Text(
                  'Introducing Yonia, with more than 6 thousand\n' +
                      'recipes and amazing features yonia.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.062),
              child: TextField(
                keyboardType: TextInputType.name,
                decoration: InputDecoration(
                  labelText: 'Full Name',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                    fontSize: 15.0,
                  ),
                  hintText: 'Ashadul Islam Samiul',
                  hintStyle: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
                style: TextStyle(color: BaseColor.red),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.062),
              child: TextField(
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: 'Email',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                    fontSize: 15.0,
                  ),
                  hintText: 'user@example.com',
                  hintStyle: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
                style: TextStyle(color: BaseColor.red),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.01)),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.062),
              child: TextField(
                decoration: InputDecoration(
                  labelText: 'Passwords',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                    fontSize: 15.0,
                  ),
                  suffixIcon: IconButton(
                    padding: EdgeInsets.zero,
                    iconSize: 15,
                    color: Colors.black54,
                    icon: Icon(Icons.remove_red_eye),
                    onPressed: () {
                      setState(() {
                        _obscureText = !_obscureText;
                      });
                    },
                  ),
                ),
                obscureText: _obscureText,
                style: TextStyle(
                  color: BaseColor.red,
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.09)),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: width * 0.062),
              padding: EdgeInsets.zero,
              height: height * 0.078,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [BaseColor.red, BaseColor.orange],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
              child: MaterialButton(
                padding: EdgeInsets.zero,
                child: Text(
                  'NEXT',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                elevation: 0.0,
                onPressed: () =>
                    Navigator.of(context).pushNamed(CountryPage.routeName),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.062),
              child: Padding(
                padding: EdgeInsets.only(top: 8.0, bottom: 16.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Already a user? ",
                      style: TextStyle(
                        fontSize: 13,
                        color: Colors.black54,
                      ),
                    ),
                    GestureDetector(
                      child: Text(
                        'Sign in',
                        style: TextStyle(
                          fontSize: 13,
                          color: Colors.black87,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      onTap: () =>
                          Navigator.of(context).pushNamed(SignInPage.routeName),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// ================================================================================
// =                              LANDSCAPESIGNUPPAGE                             =
// ================================================================================
class LandscapeSignUPPage extends StatefulWidget {
  const LandscapeSignUPPage();
  @override
  _LandscapeSignUPPageState createState() => _LandscapeSignUPPageState();
}

class _LandscapeSignUPPageState extends State<LandscapeSignUPPage> {
  bool _obscureText;
  @override
  void initState() {
    _obscureText = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      body: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            width: width * 0.35,
            height: height,
            alignment: Alignment.center,
            child: Image.asset('assets/images/start.png'),
          ),
          VerticalDivider(
            width: width * 0.0,
            thickness: 2.0,
          ),
          Container(
            width: width * 0.65,
            child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Container(
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top + 24.0,
                      left: 16.0,
                      right: 16.0,
                    ),
                    height: height * 0.08,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Sign up',
                        style: TextStyle(
                          color: BaseColor.lynch,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    height: height * 0.25,
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: FittedBox(
                      child: Text(
                        'Introducing Yonia, with more than 6 thousand\n' +
                            'recipes and amazing features yonia.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: BaseColor.botticelli,
                          fontFamily: 'Roboto',
                          wordSpacing: 2.0,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: TextField(
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        labelText: 'Full Name',
                        labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black87,
                          fontSize: 15.0,
                        ),
                        hintText: 'Ashadul Islam Samiul',
                        hintStyle: TextStyle(
                          fontSize: 15.0,
                        ),
                      ),
                      style: TextStyle(color: BaseColor.red),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: TextField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: 'Email',
                        labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black87,
                          fontSize: 15.0,
                        ),
                        hintText: 'user@example.com',
                        hintStyle: TextStyle(
                          fontSize: 15.0,
                        ),
                      ),
                      style: TextStyle(color: BaseColor.red),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    padding: EdgeInsets.only(top: 16),
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                        labelText: 'Passwords',
                        labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black87,
                          fontSize: 15.0,
                        ),
                        suffixIcon: IconButton(
                          padding: EdgeInsets.zero,
                          iconSize: 15,
                          color: Colors.black54,
                          icon: Icon(Icons.remove_red_eye),
                          onPressed: () {
                            setState(() {
                              _obscureText = !_obscureText;
                            });
                          },
                        ),
                      ),
                      obscureText: _obscureText,
                      style: TextStyle(
                        color: BaseColor.red,
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    width: width * 0.5,
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    padding: EdgeInsets.only(top: 16.0),
                    child: Container(
                      padding: EdgeInsets.zero,
                      width: width * 0.25,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [BaseColor.red, BaseColor.orange],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        ),
                      ),
                      child: MaterialButton(
                        padding: EdgeInsets.zero,
                        child: Text(
                          'NEXT',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        elevation: 0.0,
                        onPressed: () => Navigator.of(context)
                            .pushNamed(CountryPage.routeName),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    margin: EdgeInsets.only(
                      left: width * 0.062,
                      right: width * 0.062,
                      top: 16.0,
                      bottom: 16.0,
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Already a user? ",
                          style: TextStyle(
                            fontSize: 13,
                            color: Colors.black54,
                          ),
                        ),
                        GestureDetector(
                          child: Text(
                            'Sign in',
                            style: TextStyle(
                              fontSize: 13,
                              color: Colors.black87,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          onTap: () => Navigator.of(context)
                              .pushNamed(SignInPage.routeName),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:yonia/src/configs/configs.dart';
import 'package:yonia/src/pages/pages.dart';

class StartPage extends StatelessWidget {
  static const String routeName = '/StartPage';
  const StartPage();
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => orientation == Orientation.portrait
          ? PortraitStartPage()
          : LandscapeStartPage(),
    );
  }
}

// ================================================================================
// =                               PORTRAITSTARTPAGE                              =
// ================================================================================
class PortraitStartPage extends StatelessWidget {
  const PortraitStartPage();
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: MediaQuery.of(context).padding.top + 16.0,
            ),
          ),
          SliverToBoxAdapter(
            child: Center(
              child: DropdownButton(
                iconEnabledColor: BaseColor.botticelli,
                icon: Icon(Icons.keyboard_arrow_down),
                iconSize: 18,
                style: TextStyle(
                  color: BaseColor.botticelli,
                  fontFamily: 'Roboto',
                ),
                underline: Container(),
                items: [
                  DropdownMenuItem(child: Text('English (United States)')),
                  DropdownMenuItem(child: Text('German (Germany)')),
                  DropdownMenuItem(child: Text('Spanish (Spain)')),
                ],
                onChanged: (language) {},
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Image.asset(
              'assets/images/start.png',
              height: height * 0.26,
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.015)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: width * 0.23),
              height: height * 0.037,
              child: FittedBox(
                child: Text(
                  'Getting Started',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.040)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              height: height * 0.092,
              child: FittedBox(
                child: Text(
                  'Introducing Yonia, with more than 6 thousand\n' +
                      'recipes and amazing features yonia is your\n' +
                      'best choice to make any cook great.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.08)),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: ButtonBar(
                buttonMinWidth: width * 0.4,
                buttonPadding: EdgeInsets.zero,
                mainAxisSize: MainAxisSize.max,
                alignment: MainAxisAlignment.spaceBetween,
                children: [
                  OutlineButton(
                    textColor: BaseColor.botticelli,
                    child: Text(
                      'SIGN IN',
                      style: TextStyle(fontWeight: FontWeight.normal),
                    ),
                    onPressed: () =>
                        Navigator.of(context).pushNamed(SignInPage.routeName),
                  ),
                  OutlineButton(
                    textColor: BaseColor.botticelli,
                    child: Text(
                      'SIGN UP',
                      style: TextStyle(fontWeight: FontWeight.normal),
                    ),
                    onPressed: () =>
                        Navigator.of(context).pushNamed(SignUPPage.routeName),
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.01)),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: ButtonBar(
                buttonMinWidth: width,
                buttonPadding: EdgeInsets.zero,
                mainAxisSize: MainAxisSize.max,
                alignment: MainAxisAlignment.center,
                buttonTextTheme: ButtonTextTheme.normal,
                children: [
                  RaisedButton(
                    child: Text(
                      'SIGN UP WITH TWITTER',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    color: Color(0xff1da1f3),
                    elevation: 0.0,
                    hoverElevation: 0.0,
                    onPressed: () {},
                  ),
                  RaisedButton(
                    child: Text(
                      'SIGN UP WITH FACEBOOK',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    color: Color(0xff3b5998),
                    elevation: 0.0,
                    hoverElevation: 0.0,
                    onPressed: () {},
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Center(
              child: OutlineButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                padding: EdgeInsets.zero,
                textColor: BaseColor.botticelli,
                borderSide: BorderSide.none,
                child: Text(
                  'SKIP',
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 13.0),
                ),
                onPressed: () {},
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// ================================================================================
// =                              LANDSCAPESTARTPAGE                              =
// ================================================================================
class LandscapeStartPage extends StatelessWidget {
  const LandscapeStartPage();
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            // For Button bar on splash page
            margin:
                EdgeInsets.only(top: MediaQuery.of(context).padding.top + 16.0),
            alignment: Alignment.topCenter,
            width: width * 0.35,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                DropdownButton(
                  iconEnabledColor: BaseColor.botticelli,
                  icon: Icon(Icons.keyboard_arrow_down),
                  iconSize: 18,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                  ),
                  underline: Container(),
                  items: [
                    DropdownMenuItem(child: Text('English (United States)')),
                    DropdownMenuItem(child: Text('German (Germany)')),
                    DropdownMenuItem(child: Text('Spanish (Spain)')),
                  ],
                  onChanged: (language) {},
                ),
                SizedBox(height: height * 0.15),
                Image.asset('assets/images/start.png'),
              ],
            ),
          ),
          VerticalDivider(
            width: width * 0.0,
            thickness: 2.0,
          ),
          Container(
            width: width * 0.65,
            height: height,
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + 24.0,
              left: 16.0,
              right: 16.0,
            ),
            child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Container(
                    height: height * 0.08,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Getting Started',
                        style: TextStyle(
                          color: BaseColor.lynch,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    height: height * 0.25,
                    child: FittedBox(
                      child: Text(
                        'Introducing Yonia, with more than 6 thousand\n' +
                            'recipes and amazing features yonia is your\n' +
                            'best choice to make any cook great.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: BaseColor.botticelli,
                          fontFamily: 'Roboto',
                          wordSpacing: 2.0,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(child: SizedBox(height: height * 0.08)),
                SliverToBoxAdapter(
                  child: Container(
                    width: width * 0.5,
                    child: ButtonBar(
                      buttonMinWidth: width * 0.25,
                      buttonPadding: EdgeInsets.zero,
                      mainAxisSize: MainAxisSize.max,
                      alignment: MainAxisAlignment.spaceBetween,
                      children: [
                        OutlineButton(
                          textColor: BaseColor.botticelli,
                          child: Text(
                            'SIGN IN',
                            style: TextStyle(fontWeight: FontWeight.normal),
                          ),
                          onPressed: () => Navigator.of(context)
                              .pushNamed(SignInPage.routeName),
                        ),
                        OutlineButton(
                          textColor: BaseColor.botticelli,
                          child: Text(
                            'SIGN UP',
                            style: TextStyle(fontWeight: FontWeight.normal),
                          ),
                          onPressed: () => Navigator.of(context)
                              .pushNamed(SignUPPage.routeName),
                        ),
                      ],
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    margin: EdgeInsets.only(top: height * 0.02),
                    width: width * 0.5,
                    child: ButtonBar(
                      buttonMinWidth: width,
                      buttonPadding: EdgeInsets.zero,
                      mainAxisSize: MainAxisSize.max,
                      alignment: MainAxisAlignment.center,
                      buttonTextTheme: ButtonTextTheme.normal,
                      children: [
                        RaisedButton(
                          child: Text(
                            'SIGN UP WITH TWITTER',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          color: Color(0xff1da1f3),
                          elevation: 0.0,
                          hoverElevation: 0.0,
                          onPressed: () {},
                        ),
                        RaisedButton(
                          child: Text(
                            'SIGN UP WITH FACEBOOK',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          color: Color(0xff3b5998),
                          elevation: 0.0,
                          hoverElevation: 0.0,
                          onPressed: () {},
                        )
                      ],
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    width: width * 0.5,
                    // margin: EdgeInsets.symmetric(vertical: 16.0),
                    child: Center(
                      child: OutlineButton(
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        padding: EdgeInsets.zero,
                        textColor: BaseColor.botticelli,
                        borderSide: BorderSide.none,
                        child: Text(
                          'SKIP',
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 15,
                          ),
                        ),
                        onPressed: () {},
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

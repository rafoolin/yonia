import 'package:flutter/material.dart';
import 'package:yonia/src/configs/configs.dart';
import 'package:yonia/src/pages/pages.dart';

class AutoLoginPage extends StatelessWidget {
  static const String routeName = '/AutoLoginPage';
  const AutoLoginPage();
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => orientation == Orientation.portrait
          ? PortraitAutoLoginPage()
          : LandscapeAutoLoginPage(),
    );
  }
}

// ================================================================================
// =                             PORTRAITAUTOLOGINPAGE                            =
// ================================================================================
class PortraitAutoLoginPage extends StatelessWidget {
  const PortraitAutoLoginPage();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(child: SizedBox(height: height * 0.02)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: width * 0.23),
              height: height * 0.037,
              child: FittedBox(
                child: Text(
                  'Auto Login',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.01)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              height: height * 0.092,
              child: FittedBox(
                child: Text(
                  'Introducing Yonia, with more than 6 thousand\n' +
                      'recipes and amazing features yonia.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: 300.0,
              margin: EdgeInsets.only(top: 36.0, bottom: 16.0),
              alignment: Alignment.topCenter,
              child: Stack(
                children: [
                  Positioned.fill(
                    left: 16.0,
                    right: 16.0,
                    child: Image.asset(
                      'assets/images/wireFrame.png',
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  Positioned(
                    left: width * 0.2,
                    right: width * 0.2,
                    top: 200.0,
                    child: Container(
                      alignment: Alignment.center,
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: Text('User full name'),
                        subtitle: Text('Chef'),
                        leading: CircleAvatar(
                          backgroundColor: Colors.blue,
                          radius: 16.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
              child: Container(
            margin: EdgeInsets.symmetric(horizontal: 16.0),
            child: ButtonBar(
              buttonMinWidth: width * 0.4,
              buttonPadding: EdgeInsets.zero,
              mainAxisSize: MainAxisSize.max,
              alignment: MainAxisAlignment.spaceBetween,
              children: [
                OutlineButton(
                  textColor: BaseColor.botticelli,
                  child: Text(
                    'NOPE, I PASS',
                    style: TextStyle(fontWeight: FontWeight.normal),
                  ),
                  onPressed: () =>
                      Navigator.of(context).pushNamed(SignInPage.routeName),
                ),
                OutlineButton(
                  textColor: BaseColor.botticelli,
                  child: Text(
                    'REMEMBER ME',
                    style: TextStyle(fontWeight: FontWeight.normal),
                  ),
                  onPressed: () =>
                      Navigator.of(context).pushNamed(SignUPPage.routeName),
                ),
              ],
            ),
          )),
        ],
      ),
    );
  }
}

// ================================================================================
// =                            LANDSCAPEAUTOLOGINPAGE                            =
// ================================================================================
class LandscapeAutoLoginPage extends StatelessWidget {
  const LandscapeAutoLoginPage();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: width * 0.23),
              height: height * 0.08,
              child: FittedBox(
                fit: BoxFit.contain,
                child: Text(
                  'Auto Login',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: height * 0.18,
              padding: EdgeInsets.symmetric(horizontal: width * 0.2),
              margin: EdgeInsets.only(
                top: 16.0,
                left: 16.0,
                right: 16.0,
              ),
              child: FittedBox(
                child: Text(
                  'Introducing Yonia, with more than 6 thousand\n' +
                      'recipes and amazing features yonia.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: 300.0,
              margin: EdgeInsets.only(top: 36.0, bottom: 16.0),
              alignment: Alignment.topCenter,
              child: Stack(
                children: [
                  Positioned.fill(
                    left: 16.0,
                    right: 16.0,
                    child: Image.asset(
                      'assets/images/wireFrame.png',
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  Positioned(
                    left: width * 0.42,
                    right: width * 0.4,
                    top: 200.0,
                    child: Container(
                      alignment: Alignment.center,
                      child: ListTile(
                        contentPadding: EdgeInsets.zero,
                        title: Text('User full name'),
                        subtitle: Text('Chef'),
                        leading: CircleAvatar(
                          backgroundColor: Colors.blue,
                          radius: 16.0,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              width: width * 0.65,
              height: height * 0.035,
              margin: EdgeInsets.only(top: height * 0.05),
              child: ButtonBar(
                buttonMinWidth: width * 0.4,
                buttonPadding: EdgeInsets.zero,
                mainAxisSize: MainAxisSize.max,
                alignment: MainAxisAlignment.spaceBetween,
                children: [
                  OutlineButton(
                    textColor: BaseColor.botticelli,
                    child: Text(
                      'NOPE, I PASS',
                      style: TextStyle(fontWeight: FontWeight.normal),
                    ),
                    onPressed: () =>
                        Navigator.of(context).pushNamed(SignInPage.routeName),
                  ),
                  OutlineButton(
                    textColor: BaseColor.botticelli,
                    child: Text(
                      'REMEMBER ME',
                      style: TextStyle(fontWeight: FontWeight.normal),
                    ),
                    onPressed: () =>
                        Navigator.of(context).pushNamed(SignUPPage.routeName),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:yonia/src/configs/configs.dart';
import 'package:yonia/src/pages/pages.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatefulWidget {
  static const String routeName = '/SplashPage';
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  PageController _pageController;
  int _page = 0;
  @override
  void initState() {
    _pageController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Positioned.fill(
            child: PageView.builder(
              itemCount: 4,
              controller: _pageController,
              onPageChanged: (value) => setState(() => _page = value),
              itemBuilder: (context, index) {
                switch (index) {
                  case 1:
                    return const IngredientsPage();
                    break;
                  case 2:
                    return const MasterChefsPage();
                    break;
                  case 3:
                    return const DecoratingPage();
                    break;
                  case 0:
                  default:
                    return const WelcomePage();
                }
              },
            ),
          ),
          Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: ButtonBar(
              mainAxisSize: MainAxisSize.max,
              buttonPadding: EdgeInsets.zero,
              alignment: MainAxisAlignment.spaceBetween,
              children: [
                OutlineButton(
                  padding: EdgeInsets.zero,
                  textColor: BaseColor.botticelli,
                  borderSide: BorderSide.none,
                  child: Text('SKIP'),
                  onPressed: () =>
                      Navigator.of(context).pushNamed(StartPage.routeName),
                ),
                Row(
                  children: List.generate(
                    4,
                    (index) => Container(
                      width: 6.0,
                      height: 6.0,
                      margin: EdgeInsets.all(3.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(25)),
                        color:
                            _page == index ? BaseColor.red : Color(0xffd0d0d0),
                      ),
                    ),
                  ).toList(),
                ),
                OutlineButton(
                  padding: EdgeInsets.zero,
                  textColor: BaseColor.botticelli,
                  borderSide: BorderSide.none,
                  child: Text('NEXT'),
                  onPressed: () => _page == 3
                      ? Navigator.of(context)
                          .popAndPushNamed(StartPage.routeName)
                      : _pageController.animateToPage(
                          _page + 1,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.ease,
                        ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

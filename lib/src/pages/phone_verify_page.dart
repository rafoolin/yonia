import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:yonia/src/configs/configs.dart';
import 'package:yonia/src/pages/pages.dart';

class PhoneVerifyPage extends StatelessWidget {
  static const String routeName = '/PhoneVerifyPage';
  const PhoneVerifyPage();
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => orientation == Orientation.portrait
          ? PortraitPhoneVerifyPage()
          : LandscapePhoneVerifyPage(),
    );
  }
}

// ================================================================================
// =                            PORTRAITPHONEVERIFYPAGE                           =
// ================================================================================
class PortraitPhoneVerifyPage extends StatelessWidget {
  const PortraitPhoneVerifyPage();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(child: SizedBox(height: height * 0.02)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: width * 0.23),
              height: height * 0.037,
              child: FittedBox(
                child: Text(
                  'Verify your mobile',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.01)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              height: height * 0.092,
              child: FittedBox(
                child: Text(
                  'Waiting to automatically detect and SMS sent to\n' +
                      '+880 1515638061. Wrong number?',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.01)),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.062),
              child: PinCodeTextField(
                appContext: context,
                length: 4,
                onChanged: (value) {},
                pinTheme: PinTheme(
                  inactiveColor: Colors.black38,
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.062),
              child: Padding(
                padding: EdgeInsets.only(top: 8.0),
                child: Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    child: Text(
                      'Enter 4-digit code',
                      style: TextStyle(fontSize: 12, color: Colors.black45),
                      textAlign: TextAlign.center,
                    ),
                    onTap: () =>
                        Navigator.of(context).pushNamed(ForgotPage.routeName),
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.04)),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: width * 0.062),
              padding: EdgeInsets.zero,
              height: height * 0.078,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [BaseColor.red, BaseColor.orange],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
              child: MaterialButton(
                padding: EdgeInsets.zero,
                child: Text(
                  'VERIFY NOW ',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                elevation: 0.0,
                onPressed: () =>
                    Navigator.of(context).pushNamed(AutoLoginPage.routeName),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// ================================================================================
// =                           LANDSCAPEPHONEVERIFYPAGE                           =
// ================================================================================
class LandscapePhoneVerifyPage extends StatelessWidget {
  const LandscapePhoneVerifyPage();
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: width * 0.23),
              height: height * 0.08,
              child: FittedBox(
                fit: BoxFit.contain,
                child: Text(
                  'Verify your mobile',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: height * 0.18,
              padding: EdgeInsets.symmetric(horizontal: width * 0.2),
              margin: EdgeInsets.only(
                top: 16.0,
                left: 16.0,
                right: 16.0,
              ),
              child: FittedBox(
                child: Text(
                  'Waiting to automatically detect and SMS sent to\n' +
                      '+880 1515638061. Wrong number?',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.1)),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.3),
              child: PinCodeTextField(
                appContext: context,
                length: 4,
                onChanged: (value) {},
                pinTheme: PinTheme(
                  inactiveColor: Colors.black38,
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              width: width * 0.65,
              height: height * 0.035,
              margin: EdgeInsets.only(top: height * 0.05),
              child: FittedBox(
                child: Align(
                  alignment: Alignment.center,
                  child: GestureDetector(
                    child: Text(
                      'Enter 4-digit code',
                      style: TextStyle(fontSize: 12, color: Colors.black45),
                      textAlign: TextAlign.center,
                    ),
                    onTap: () =>
                        Navigator.of(context).pushNamed(ForgotPage.routeName),
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              width: width * 0.5,
              margin: EdgeInsets.only(
                top: height * 0.05,
                left: 16.0,
                right: 16.0,
              ),
              padding: EdgeInsets.only(top: 16.0),
              child: Container(
                padding: EdgeInsets.zero,
                width: width * 0.25,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [BaseColor.red, BaseColor.orange],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                ),
                child: MaterialButton(
                  padding: EdgeInsets.zero,
                  child: Text(
                    'VERIFY NOW ',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  elevation: 0.0,
                  onPressed: () =>
                      Navigator.of(context).pushNamed(AutoLoginPage.routeName),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

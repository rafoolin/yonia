import 'package:flutter/material.dart';
import 'package:yonia/src/configs/configs.dart';
import 'package:yonia/src/pages/pages.dart';

class VerificationPage extends StatelessWidget {
  static const String routeName = '/VerificationPage';
  const VerificationPage();
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => orientation == Orientation.portrait
          ? PortraitVerificationPage()
          : LandscapeVerificationPage(),
    );
  }
}

// ================================================================================
// =                           PORTRAITVERIFICATIONPAGE                           =
// ================================================================================
class PortraitVerificationPage extends StatelessWidget {
  const PortraitVerificationPage();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: MediaQuery.of(context).padding.top + 16.0,
            ),
          ),
          SliverToBoxAdapter(
            child: Center(
              child: DropdownButton(
                iconEnabledColor: BaseColor.botticelli,
                icon: Icon(Icons.keyboard_arrow_down),
                iconSize: 18,
                style: TextStyle(
                  color: BaseColor.botticelli,
                  fontFamily: 'Roboto',
                ),
                underline: Container(),
                items: [
                  DropdownMenuItem(child: Text('English (United States)')),
                  DropdownMenuItem(child: Text('German (Germany)')),
                  DropdownMenuItem(child: Text('Spanish (Spain)')),
                ],
                onChanged: (language) {},
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Image.asset(
              'assets/images/start.png',
              height: height * 0.2,
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.02)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: width * 0.23),
              height: height * 0.037,
              child: FittedBox(
                child: Text(
                  'User Verification',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.01)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              height: height * 0.092,
              child: FittedBox(
                child: Text(
                  'Introducing Yonia, with more than 6 thousand\n' +
                      'recipes and amazing features yonia.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.062),
              child: TextFormField(
                enabled: false,
                initialValue: 'Bangladesh',
                decoration: InputDecoration(
                  labelText: 'Country',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                    fontSize: 15.0,
                  ),
                ),
                style: TextStyle(color: BaseColor.red),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.062),
              child: TextField(
                keyboardType: TextInputType.phone,
                decoration: InputDecoration(
                  labelText: 'Phone Number',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black87,
                    fontSize: 15.0,
                  ),
                  suffixIcon:
                      Icon(Icons.check, color: Colors.greenAccent, size: 15),
                ),
                style: TextStyle(
                  color: BaseColor.red,
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(top: 36.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'By signing up you agree to our ',
                    style: TextStyle(fontSize: 12, color: Colors.black45),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 8.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        child: Text(
                          'Terms & Conditions ',
                          style: TextStyle(
                            fontSize: 12,
                            color: BaseColor.red,
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        onTap: () {},
                      ),
                      Text(
                        'and ',
                        style: TextStyle(fontSize: 12, color: Colors.black45),
                        textAlign: TextAlign.center,
                      ),
                      GestureDetector(
                        child: Text(
                          'Privacy Policy',
                          style: TextStyle(
                            fontSize: 12,
                            color: BaseColor.red,
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        onTap: () {},
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.04)),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: width * 0.062),
              padding: EdgeInsets.zero,
              height: height * 0.078,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [BaseColor.red, BaseColor.orange],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
              child: MaterialButton(
                padding: EdgeInsets.zero,
                child: Text(
                  'SIGN UP NOW',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                elevation: 0.0,
                onPressed: () =>
                    Navigator.of(context).pushNamed(PhoneVerifyPage.routeName),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// ================================================================================
// =                           LANDSCAPEVERIFICATIONPAGE                          =
// ================================================================================
class LandscapeVerificationPage extends StatelessWidget {
  const LandscapeVerificationPage();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            width: width * 0.35,
            child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Container(
                    alignment: Alignment.topCenter,
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).padding.top + 16.0),
                    child: DropdownButton(
                      iconEnabledColor: BaseColor.botticelli,
                      icon: Icon(Icons.keyboard_arrow_down),
                      iconSize: 18,
                      style: TextStyle(
                        color: BaseColor.botticelli,
                        fontFamily: 'Roboto',
                      ),
                      underline: Container(),
                      items: [
                        DropdownMenuItem(
                            child: Text('English (United States)')),
                        DropdownMenuItem(child: Text('German (Germany)')),
                        DropdownMenuItem(child: Text('Spanish (Spain)')),
                      ],
                      onChanged: (language) {},
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: SizedBox(height: height * 0.15),
                ),
                SliverToBoxAdapter(
                  child: Image.asset('assets/images/start.png'),
                )
              ],
            ),
          ),
          VerticalDivider(
            width: width * 0.0,
            thickness: 2.0,
          ),
          Container(
            width: width * 0.65,
            child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Container(
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top + 24.0,
                      left: 16.0,
                      right: 16.0,
                    ),
                    height: height * 0.08,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'User Verification',
                        style: TextStyle(
                          color: BaseColor.lynch,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    height: height * 0.25,
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: FittedBox(
                      child: Text(
                        'Introducing Yonia, with more than 6 thousand\n' +
                            'recipes and amazing features yonia.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: BaseColor.botticelli,
                          fontFamily: 'Roboto',
                          wordSpacing: 2.0,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: TextFormField(
                      enabled: false,
                      initialValue: 'Bangladesh',
                      decoration: InputDecoration(
                        labelText: 'Country',
                        labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black87,
                          fontSize: 15.0,
                        ),
                      ),
                      style: TextStyle(color: BaseColor.red),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    padding: EdgeInsets.only(top: 16),
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: TextField(
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        labelText: 'Phone Number',
                        labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black87,
                          fontSize: 15.0,
                        ),
                        suffixIcon: Icon(Icons.check,
                            color: Colors.greenAccent, size: 15),
                      ),
                      style: TextStyle(
                        color: BaseColor.red,
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    padding: EdgeInsets.only(top: 8.0),
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        child: Text(
                          'Forgot Passwords?',
                          style: TextStyle(fontSize: 12, color: Colors.black45),
                        ),
                        onTap: () => Navigator.of(context)
                            .pushNamed(ForgotPage.routeName),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    width: width * 0.65,
                    margin: EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'By signing up you agree to our ',
                          style: TextStyle(fontSize: 12, color: Colors.black45),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 8.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              child: Text(
                                'Terms & Conditions ',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: BaseColor.red,
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              onTap: () {},
                            ),
                            Text(
                              'and ',
                              style: TextStyle(
                                  fontSize: 12, color: Colors.black45),
                              textAlign: TextAlign.center,
                            ),
                            GestureDetector(
                              child: Text(
                                'Privacy Policy',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: BaseColor.red,
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              onTap: () {},
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    width: width * 0.5,
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    padding: EdgeInsets.only(top: 16.0),
                    child: Container(
                      padding: EdgeInsets.zero,
                      width: width * 0.25,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [BaseColor.red, BaseColor.orange],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        ),
                      ),
                      child: MaterialButton(
                        padding: EdgeInsets.zero,
                        child: Text(
                          'SIGN UP NOW',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        elevation: 0.0,
                        onPressed: () => Navigator.of(context)
                            .pushNamed(PhoneVerifyPage.routeName),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:yonia/src/configs/configs.dart';
import 'package:yonia/src/pages/pages.dart';

class SentEmailPage extends StatelessWidget {
  static const String routeName = '/SentEmailPage';
  const SentEmailPage();
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => orientation == Orientation.portrait
          ? PortraitSentEmailPage()
          : LandscapeSentEmailPage(),
    );
  }
}

// ================================================================================
// =                             PORTRAITSENTEMAILPAGE                            =
// ================================================================================
class PortraitSentEmailPage extends StatefulWidget {
  const PortraitSentEmailPage();
  @override
  _PortraitSentEmailPageState createState() => _PortraitSentEmailPageState();
}

class _PortraitSentEmailPageState extends State<PortraitSentEmailPage> {
  GlobalKey<ScaffoldState> _key;
  @override
  void initState() {
    _key = GlobalKey();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _key,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: MediaQuery.of(context).padding.top + 24.0,
            ),
          ),
          SliverToBoxAdapter(
            child: Image.asset(
              'assets/images/sent.png',
              height: height * 0.1,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(top: 16.0),
              height: height * 0.04,
              child: FittedBox(
                child: Text(
                  'We’ve sent you an email',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.all(16.0),
              height: height * 0.055,
              child: FittedBox(
                child: Text(
                  'We have sent you and email with a link\n' +
                      'to change your account password.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.05)),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.all(16.0),
              padding: EdgeInsets.zero,
              height: height * 0.078,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [BaseColor.red, BaseColor.orange],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
              child: MaterialButton(
                padding: EdgeInsets.zero,
                child: Text(
                  'BACK TO LOGIN PAGE',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                elevation: 0.0,
                onPressed: () =>
                    Navigator.of(context).pushNamed(SignInPage.routeName),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(bottom: 16.0),
              child: GestureDetector(
                child: Center(
                  child: Text(
                    'Resend email',
                    style: TextStyle(
                      fontSize: 13,
                      color: Colors.black54,
                    ),
                  ),
                ),
                onTap: () {
                  _key.currentState.showSnackBar(
                      SnackBar(content: Text('We sent you another email')));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// ================================================================================
// =                            LANDSCAPESENTEMAILPAGE                            =
// ================================================================================
class LandscapeSentEmailPage extends StatefulWidget {
  const LandscapeSentEmailPage();
  @override
  _LandscapeSentEmailPageState createState() => _LandscapeSentEmailPageState();
}

class _LandscapeSentEmailPageState extends State<LandscapeSentEmailPage> {
  GlobalKey<ScaffoldState> _key;

  @override
  void initState() {
    _key = GlobalKey();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      key: _key,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(
              height: MediaQuery.of(context).padding.top + 24.0,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              alignment: Alignment.center,
              width: width * 0.3,
              height: height * 0.2,
              child: Image.asset('assets/images/sent.png'),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              margin: EdgeInsets.only(
                top: 8.0,
                left: 16.0,
                right: 16.0,
              ),
              height: height * 0.08,
              child: FittedBox(
                fit: BoxFit.contain,
                child: Text(
                  'We’ve sent you an email',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.05)),
          SliverToBoxAdapter(
            child: Container(
              height: height * 0.13,
              margin: EdgeInsets.only(
                top: 8.0,
                left: 16.0,
                right: 16.0,
              ),
              child: FittedBox(
                child: Text(
                  'We have sent you and email with a link\n' +
                      'to change your account password.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              width: width * 0.5,
              margin: EdgeInsets.only(
                top: height * 0.05,
                left: 16.0,
                right: 16.0,
              ),
              padding: EdgeInsets.only(top: 16.0),
              child: Container(
                padding: EdgeInsets.zero,
                width: width * 0.25,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [BaseColor.red, BaseColor.orange],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                ),
                child: MaterialButton(
                  padding: EdgeInsets.zero,
                  child: Text(
                    'BACK TO LOGIN PAGE',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  elevation: 0.0,
                  onPressed: () =>
                      Navigator.of(context).pushNamed(SignInPage.routeName),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 16.0),
              child: GestureDetector(
                child: Center(
                  child: Text(
                    'Resend email',
                    style: TextStyle(
                      fontSize: 13,
                      color: Colors.black54,
                    ),
                  ),
                ),
                onTap: () {
                  _key.currentState.showSnackBar(
                      SnackBar(content: Text('We sent you another email')));
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

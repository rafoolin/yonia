import 'package:flutter/material.dart';
import 'package:yonia/src/configs/configs.dart';
import 'package:yonia/src/pages/pages.dart';

class CountryPage extends StatelessWidget {
  static const String routeName = '/CountryPage';
  const CountryPage();
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => orientation == Orientation.portrait
          ? PortraitCountryPage()
          : LandscapeCountryPage(),
    );
  }
}

// ================================================================================
// =                              PORTRAITCOUNTRYPAGE                             =
// ================================================================================
class PortraitCountryPage extends StatelessWidget {
  const PortraitCountryPage();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: SizedBox(height: MediaQuery.of(context).padding.top),
          ),
          SliverToBoxAdapter(
            child: Image.asset(
              'assets/images/start.png',
              height: height * 0.18,
            ),
          ),
          SliverToBoxAdapter(
            child: Container(
              height: height * 0.045,
              child: FittedBox(
                child: Text(
                  'Select Country',
                  style: TextStyle(
                    color: BaseColor.lynch,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(child: SizedBox(height: height * 0.01)),
          SliverToBoxAdapter(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              height: height * 0.092,
              child: FittedBox(
                child: Text(
                  'Introducing Yonia, with more than 6 thousand\n' +
                      'recipes and amazing features yonia.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: BaseColor.botticelli,
                    fontFamily: 'Roboto',
                    wordSpacing: 2.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: 20,
              itemBuilder: (context, index) => ListTile(
                title: Text('Country'),
                onTap: () =>
                    Navigator.of(context).pushNamed(VerificationPage.routeName),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// ================================================================================
// =                             LANDSCAPECOUNTRYPAGE                             =
// ================================================================================
class LandscapeCountryPage extends StatelessWidget {
  const LandscapeCountryPage();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black45),
        elevation: 0.0,
      ),
      body: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            width: width * 0.35,
            height: height,
            alignment: Alignment.center,
            child: Image.asset('assets/images/start.png'),
          ),
          VerticalDivider(
            width: width * 0.0,
            thickness: 2.0,
          ),
          Container(
            width: width * 0.65,
            child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Container(
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).padding.top + 24.0,
                      left: 16.0,
                      right: 16.0,
                    ),
                    height: height * 0.08,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        'Select Country',
                        style: TextStyle(
                          color: BaseColor.lynch,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    height: height * 0.25,
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: FittedBox(
                      child: Text(
                        'Introducing Yonia, with more than 6 thousand\n' +
                            'recipes and amazing features yonia.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: BaseColor.botticelli,
                          fontFamily: 'Roboto',
                          wordSpacing: 2.0,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: 20,
                      itemBuilder: (context, index) => ListTile(
                        title: Text('Country'),
                        onTap: () => Navigator.of(context)
                            .pushNamed(VerificationPage.routeName),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:yonia/src/configs/configs.dart';

class IngredientsPage extends StatelessWidget {
  const IngredientsPage();
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => orientation == Orientation.portrait
          ? PortraitIngredientsPage()
          : LandscapeIngredientsPage(),
    );
  }
}

// ================================================================================
// =                            PORTRAITINGREDIENTSPAGE                           =
// ================================================================================

/// Portrait orientation for Ingredients Page
class PortraitIngredientsPage extends StatelessWidget {
  const PortraitIngredientsPage();
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            padding: EdgeInsets.only(
              left: width * 0.15,
              right: width * 0.15,
              top: height * 0.132,
              bottom: height * 0.132,
            ),
            height: height * 0.6,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [BaseColor.red, BaseColor.orange],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            child: Image.asset('assets/images/ingredients.png'),
          ),
          SizedBox(height: height * 0.092),
          Container(
            padding: EdgeInsets.symmetric(horizontal: width * 0.23),
            height: height * 0.037,
            child: FittedBox(
              child: Text(
                'Available ingredients',
                style: TextStyle(
                  color: BaseColor.lynch,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
          SizedBox(height: height * 0.040),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            height: height * 0.092,
            child: FittedBox(
              child: Text(
                'Introducing Yonia, with more than 6 thousand\n' +
                    'recipes and amazing features yonia is your\n' +
                    'best choice to make any cook great.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: BaseColor.botticelli,
                  fontFamily: 'Roboto',
                  wordSpacing: 2.0,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
// ================================================================================
// =                           LANDSCAPEINGREDIENTSPAGE                           =
// ================================================================================

/// Landscape orientation for Ingredients Page
class LandscapeIngredientsPage extends StatelessWidget {
  const LandscapeIngredientsPage();
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            // For Button bar on splash page
            margin: EdgeInsets.only(bottom: height * 0.1),
            alignment: Alignment.center,
            width: width * 0.3,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [BaseColor.red, BaseColor.orange],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            child: Image.asset('assets/images/ingredients.png'),
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.all(16.0),
                height: height * 0.2,
                width: width * 0.7,
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: Text(
                    'Available ingredients',
                    style: TextStyle(
                      color: BaseColor.lynch,
                      fontFamily: 'Roboto',
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
              Container(
                height: height * 0.3,
                width: width * 0.7,
                padding: EdgeInsets.all(16.0),
                child: FittedBox(
                  child: Text(
                    'Introducing Yonia, with more than 6 thousand\n' +
                        'recipes and amazing features yonia is your\n' +
                        'best choice to make any cook great.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: BaseColor.botticelli,
                      fontFamily: 'Roboto',
                      wordSpacing: 2.0,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

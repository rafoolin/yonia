import 'package:flutter/material.dart';

class BaseColor {
  static final Color lynch = Color(0xff68738b);
  static final Color orange = Color(0xffff8400);
  static final Color red = Color(0xfffb3d3c);
  static final Color botticelli = Color(0xff90a4ae);
}
